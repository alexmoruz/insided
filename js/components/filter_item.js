define(['jquery'], function ($) {

    var rowTmpl = $('#filterRowTemplate').html();
    var tagTmpl = $('#filterTagTemplate').html();

    var $filterListContainer = $('.js-filters-list');
    var $filterTagsContainer = $('.js-tags-list');

    function Filter (data, onRemove) {
        this.data = data;
        this.onRemove = onRemove;
        this.init();
    }

    $.extend(Filter.prototype, {
        init: function () {
            // get templates
            this.$el = $(rowTmpl);
            this.$tag = $(tagTmpl);

            // append elements to contaner
            $filterListContainer.append(this.$el);
            $filterTagsContainer.append(this.$tag);

            // initialize all fileds
            this.$column = $('.js-filter-column', this.$el);
            this.$comparation_number = $('.js-filter-comparation-number', this.$el);
            this.$comparation_time = $('.js-filter-comparation-time', this.$el);
            this.$usergroup = $('.js-filter-usergroup', this.$el);
            this.$date = $('.js-filter-date', this.$el);

            this.changeItems();

            // set defalut values
            this.setSelectValue(this.data.column, this.$column);
            this.setSelectValue(this.data.comparation_number, this.$comparation_number);
            this.setSelectValue(this.data.comparation_time, this.$comparation_time);
            this.setSelectValue(this.data.usergroup, this.$usergroup);
            if (this.data.date) {
                $('input', this.$date).val(this.data.date);
            }

            this.setTagText();
            this.setEvents();
        },

        // set default values for select field
        setSelectValue: function (value, $container) {
            var values = [];
            if (value) {
                if (value instanceof Array && $('select', $container).is('[multiple]')) {
                    values = value;
                } else {
                    values.push(value);
                }
                values.forEach(function (val) {
                    console.log($('option[value="'+ val +'"]', $container)[0], val);
                    //
                    $('option[value="'+ val +'"]', $container).prop('selected', true);
                    // $('select', $container).val(val)
                });
            }
        },

        // set text to tag button
        setTagText: function () {
            if (this.data.column) {
                var text = $('option:selected', this.$column).text();
                $('.js-filter-tag-text', this.$tag).text(text);
            }
        },

        // append events
        setEvents: function () {
            this.handleChangeColumn = this.handleChangeColumn.bind(this);
            this.handleChangeComparationTime = this.handleChangeComparationTime.bind(this);
            this.handleRemove = this.handleRemove.bind(this);

            $('.js-remove-filter', this.$el).on('click', this.handleRemove);
            this.$tag.on('click', this.handleRemove);
            $('select', this.$column).on('change', this.handleChangeColumn);
            $('select', this.$comparation_time).on('change', this.handleChangeComparationTime);
        },

        handleChangeComparationTime: function (event) {
            var hasValue = Boolean($(event.target).val())
            $('input', this.$date).prop('disabled', !hasValue);
        },

        handleChangeColumn: function (event) {
            var val = $(event.target).val();
            if (val !== this.data.column) {
                this.data.column = val;
                this.changeItems();
                this.setTagText();
            }
        },

        handleRemove: function (event) {
            event.preventDefault();
            this.remove();
        },

        changeItems: function () {
            var c = this.data.column;

            this.$column.show();
            this.$comparation_number.hide();
            this.$comparation_time.hide();
            this.$usergroup.hide();
            this.$date.hide();

            if (c === 'comments') {
                this.$comparation_number.show();
                // $('option:selected', this.$comparation_number).prop('selected', false);
            } else if (c === 'reg_date') {
                this.$comparation_time.show();
                this.$date.show();
                $('input', this.$date).val('')
                    .prop('disabled', true);
                // $('option:selected', this.$comparation_time).prop('selected', false);
            } else if (c === 'usergroup') {
                this.$usergroup.show();
            }
        },

        remove: function () {
            // remove envents
            $('select', this.$column).off('change', this.handleChangeColumn);
            $('select', this.$comparation_time).off('change', this.handleChangeComparationTime);
            $('.js-remove-filter', this.$el).off('click', this.handleRemove);
            this.$tag.off('click', this.handleRemove);

            this.onRemove(this);

            // remove elements
            this.$tag.remove();
            this.$el.remove();
        }
    });
    return Filter;
});
