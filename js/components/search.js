define(['jquery', 'components/filter_item'], function ($, Filter) {

    // default input in advanced search
    var initial_state = [
        {
            column: 'comments',
            comparation_number: 'greater',
            comparation_time: '',
            usergroup: '',
            date: ''
        },
        {
            column: 'reg_date',
            comparation_number: 'greater',
            comparation_time: '',
            usergroup: '',
            date: ''
        },
        {
            column: 'usergroup',
            comparation_number: 'greater',
            comparation_time: '',
            usergroup: ['option_1_for_group_1', 'forum'],
            date: ''
        }
    ];

    // list of filter instances
    var filters = [];

    // default date dor new added filter
    var default_filter_data = {
        column: 'comments',
        comparation_number: 'greater',
        comparation_time: '',
        usergroup: '',
        date: ''
    }

    // create default forms
    initial_state.forEach(function (data) {
        filters.push(new Filter(data, onRemoveCallback));
    });

    // append events
    $('.js-add-filter').on('click', handleAddFilter);
    $('.js-filter-clear').on('click', handleRemoveAllFilters);

    function handleRemoveAllFilters (event) {
        event.preventDefault();
        while(filters.length > 0) {
            filters.shift().remove();
        };
    }

    function handleAddFilter (event) {
        event.preventDefault();
        var filter = new Filter(default_filter_data, onRemoveCallback);
        filters.push(filter);
        toggleTagsClear();
    }

    // remove filter from list
    function onRemoveCallback (filter) {
        var idx = filters.indexOf(filter);
        if (idx >= 0) {
            filters.splice(idx, 1);
        }
        toggleTagsClear();
    }

    // toggle clear filter button
    function toggleTagsClear () {
        var hasFilters = Boolean(filters.length);
        $('.js-filter-clear').toggle(hasFilters);
        $('.js-filter-any').toggle(!hasFilters);
    }

});
