define(['jquery'], function ($) {

    var pluginName = 'dropdown';

    var defaults = {

    };

    function Plugin(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this._init();
    }

    $.extend(Plugin.prototype, {
        _init: function () {
            this.isOpend = false;
            this.$el = $('<div class="c-dropdown">\
                <div class="c-dropdown__field"></div>\
                <div class="c-dropdown__selected">\
                    <div class="c-dropdown__selected-text">Comments</div>\
                    <div class="c-dropdown__arrow">\
                        <i class="c-icon c-icon--arrow-down"></i>\
                    </div>\
                </div>\
                <div class="c-dropdown__body" tabindex="0">\
                    <div class="c-dropdown__body-content"></div>\
                </div>\
            </div>');

            $(this.element).after(this.$el)

            // create body compoent
            this.component = this.settings.component.call(this)
            this._setComponent(this.component);

            // put field inside of custom dropdown
            $('.c-dropdown__field', this.$el).html(this.element);

            // key control dropdown
            $('.c-dropdown__body', this.$el).on('keydown', this._keyEvent.bind(this));

            /*
                // var $field = $(this.element)
                //     .attr('tabindex', '-1'); // remove from tab order
                //
                // var $wrapper =
                //
                // $field.after($wrapper);
                //
                // $('.c-dropdown__field', $wrapper).html($field);
                //
                // var $selected = $('.c-dropdown__selected-text', $wrapper);
                //
                // $('.c-dropdown__body', $wrapper).on({
                //     'keydown': function (e) {
                //         console.log(e);
                //     }
                // })
                //
                // this.settings.initBody({
                //     getField: function () {
                //         return $field;
                //     },
                //     setValue: function (val, is_placeholder) {
                //         if (is_placeholder) {
                //             $selected.addClass('c-dropdown__selected-text--placeholder');
                //         } else {
                //             $selected.removeClass('c-dropdown__selected-text--placeholder');
                //         }
                //         $selected.text(val);
                //     }
                // });
            */
        },
        _setComponent: function (domElement) {
            $('.c-dropdown__body-content', this.$el).html(domElement);
        },
        _keyEvent: function (event) {
            
            if (this.isOpend) {
                $(this.component).trigger('dropdown.keyEvent',[event])
            }

            // open if drop down is close and was presed space key
            if (!this.isOpend && event.keyCode === 32) {
                this.toggleDropdown('open');
            }
        },
        toggleDropdown: function (action) {
            switch (action) {
                case 'open':
                    this.isOpend = true;
                    $('.c-dropdown__body-content', this.$el).show();
                    break;
                case 'close':
                    this.isOpend = false;
                    $('.c-dropdown__body-content', this.$el).hide();
                    break;
                default:
                    $('.c-dropdown__body-content', this.$el)[this.isOpend ? 'hide' : 'show']();
                    this.isOpend = !this.isOpend;
            }
            if (action) {

            } else {

            }
        },
        getField: function () {
            return this.element;
        }
    });

    $.fn[ pluginName ] = function( options ) {
        return this.each(function() {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            }
        });
    };

        /*
        <div class="c-dropdown js-dropdown">
            <div class="c-dropdown__selected">
                <div class="c-dropdown__selected-text c-dropdown__selected-text--placeholder">Comments</div>
                <div class="c-dropdown__arrow">
                    <i class="c-icon c-icon--arrow-down"></i>
                </div>
            </div>
            <div class="c-dropdown__body">
                <ul class="c-dropdown__options_list">
                    <li class="c-dropdown__option">
                        Registered user
                    </li>
                    <li class="c-dropdown__option">
                        Super-user
                    </li>
                    <li class="c-dropdown__option">Moderator</li>
                </ul>
            </div>
        </div>
        */

});

// define(['jquery'], function ($) {
//
//     function Dropdown() {
//
//     }
//
//     $.extend(Dropdown.prototype, {
//         init: function () {
//
//         },
//
//     });
//
//     $.fn.dropdown = function (options) {
//         var settings = $.extend({}, options);
//
//         var dropdown_structure_str = '\
//             <div class="c-dropdown__selected">\
//                 <div class="c-dropdown__selected-text c-dropdown__selected-text--placeholder">Comments Comments Comments Comments</div>\
//                 <div class="c-dropdown__arrow">\
//                     <i class="c-icon c-icon--arrow-down"></i>\
//                 </div>\
//             </div>\
//             <div class="c-dropdown__body" tabindex="1">\
//                 <ul class="c-dropdown__options_list"></ul>\
//             </div>\
//         ';
//
//         this.each(function () {
//             var is_opened = false;
//             var $el = $(this);
//             $el.addClass('u-hide')
//                 .attr('tabindex', '-1');
//
//             $el.wrap('<div class="c-dropdown">');
//
//             var $main = $el.parent('.c-dropdown');
//             $main.append(dropdown_structure_str);
//
//             var $options_list = $('.c-dropdown__options_list', $main);
//             var placeholder_text = '';
//             $('option',$el).each(function () {
//                 var $option = $(this);
//                 var val = $option.val();
//                 var text = $option.text();
//
//                 if (val) {
//                     $options_list.append(
//                         $('<li class="c-dropdown__option">')
//                             .attr('data-value', val)
//                             .attr('title', text)
//                             .text(text)
//                     );
//                 } else {
//                     placeholder_text = text;
//                 }
//
//             });
//
//             $options_list.on('click', 'li', function (e) {
//                 e.preventDefault();
//                 e.stopPropagation();
//
//                 console.log(this);
//             });
//
//             var $list = $('.c-dropdown__options_list', $main);
//             var $body = $('.c-dropdown__body', $main);
//
//             $body.on('blur', function () {
//                 $list.hide();
//             });
//
//             $('.c-dropdown__selected', $main).on('click', function (e) {
//                 e.preventDefault();
//                 e.stopPropagation();
//                 if (is_opened) {
//                     // is opened
//                     $list.hide();
//                 } else {
//                     // is closed
//                     $body.focus()
//                     $list.show();
//
//                 }
//                 is_opened = !is_opened;
//             });
//
//             // <div class="c-dropdown js-dropdown">
//             //     <div class="c-dropdown__selected">
//             //         <div class="c-dropdown__selected-text c-dropdown__selected-text--placeholder">Comments</div>
//             //         <div class="c-dropdown__arrow">
//             //             <i class="c-icon c-icon--arrow-down"></i>
//             //         </div>
//             //     </div>
//                 // <div class="c-dropdown__options">
//                 //     <ul class="c-dropdown__options_list">
//                 //         <li class="c-dropdown__option">
//                 //             Registered user
//                 //         </li>
//                 //         <li class="c-dropdown__option">
//                 //             Super-user
//                 //         </li>
//                 //         <li class="c-dropdown__option">Moderator</li>
//                 //     </ul>
//                 // </div>
//             // </div>
//
//         })
//
//     };
// });
/*
<div class="c-dropdown">
    <div class="c-dropdown__selected">
        <div class="js-dropdown-selected">Comments</div>
        <div class="c-dropdown__arrow">
            <i class="c-icon c-icon--arrow-down"></i>
        </div>
    </div>
    <div class="c-dropdown__options">
        <ul class="c-dropdown__options_list">
            <li class="c-dropdown__option">
                Registered user
            </li>
            <li class="c-dropdown__option">
                Super-user
            </li>
            <li class="c-dropdown__option">Moderator</li>
        </ul>
    </div>
</div>
*/


// define(['jquery'], function ($) {

    // $.fn.dropdown = function (options) {
    //
    //     var dropdown_str = '\
    //         <div class="c-dropdown">\
    //             <div class="c-dropdown__selected">\
    //                 <div class="c-dropdown__selected-text"></div>\
    //                 <div class="c-dropdown__arrow">\
    //                     <i class="c-icon c-icon--arrow-down"></i>\
    //                 </div>\
    //             </div>\
    //             <div class="c-dropdown__body"></div>\
    //         </div>\
    //     ';
    //
    //     var option_str = '<li class="c-dropdown__option"></li>';
    //
    //     var simple_options_str = '<ul class="c-dropdown__options_list"></ul>';
    //
    //     var settings = $.extend({
    //         type: 'simple' // simple, multiple, date
    //     }, options);
    //
    //     this.each(function () {
    //         var $field = $(this);
    //         var $dropdown = $(dropdown_str);
    //         var $curent_value = $('.c-dropdown__selected-text', $dropdown);
    //         var $body = $('.c-dropdown__body', $dropdown);
    //
    //
    //         function changeSelected(text) {
    //             $curent_value.text(text);
    //         }
    //
    //         var component = new SimpleOptions({
    //             field: $field,
    //             body: $body,
    //             onOpen: function () {
    //
    //             },
    //             onClose: function () {
    //
    //             },
    //             setPlaceholder: function () {
    //
    //             },
    //             setValue: function () {
    //
    //             }
    //         })
    //     });
    //
    //     function SimpleOptions (opts) {
    //         var $field = opts.field;
    //         if (!$field.is('select')) {
    //             throw Error('Field is not select type');
    //         }
    //         var $compoent = $(simple_options_str);
    //         var $body = opts.body;
    //
    //         var $options = $('option', $field);
    //         $body.append($compoent);
    //         $options.each(function () {
    //             console.log(this);
    //         })
    //     }
    // };

// });
