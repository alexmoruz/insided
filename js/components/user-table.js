define(['jquery'], function ($) {

    var $table = $('.js-user-table');
    var $checkboxes = $('.js-user-table__item-checkbox');
    var $baseCheckbox = $('.js-user-table__base-checkbox');

    $baseCheckbox.on('change', function () {
        var val = $(this).prop('checked');
        $checkboxes.prop('checked', val).trigger('change');
    });

    $table.on('change', '.js-user-table__item-checkbox', function () {
        var $el = $(this);
        var val = $el.prop('checked');
        var $parent_row = $el.closest('.js-user-table__row');
        var existsChecked = Boolean($checkboxes.filter(':checked').length);
        $baseCheckbox.prop('checked', existsChecked);
        $parent_row.toggleClass('is-user-table__row--selected', val);
        if (existsChecked) {
            $('.js-user-actions').show();
        } else {
            $('.js-user-actions').hide();
        }

    });

});
