var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require("gulp-rename");
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function () {
    return gulp
        .src('sass/index.sass')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(rename('style.css'))
        .pipe(gulp.dest('dest'))
});

gulp.task('watch', function () {
    gulp.watch('sass/**/*.*', gulp.series('sass'));
});

gulp.task('dev', gulp.series('sass', 'watch'));
